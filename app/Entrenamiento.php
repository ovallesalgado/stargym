<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrenamiento extends Model
{
    protected $table="entrenamientos";
    
    protected $fillable = [
        'id',
        'rutina_id',
        'ejercicio_id',
        'series',
        'repeticiones',

    ];

    public function rutina(){
        return $this->belongsTo('App\Rutina');
    }

    public function ejercicio(){
        return $this->belongsTo('App\Ejercicio');
    }
}
