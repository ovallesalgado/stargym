<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rutina extends Model
{
    protected $table="rutinas";
    
    protected $fillable = [
        'id','nombre','descripcion',
    ];

    
    public function users(){
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function entrenamientos(){
        return $this->hasMany('App\Entrenamientos');
    }

   

}
