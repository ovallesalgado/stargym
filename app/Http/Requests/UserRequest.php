<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documento'=>'integer|unique:users|required|',
            'nombre'=>'min:4|max:120|alpha_dash|required',
            'apellido'=>'min:4|max:120|alpha_dash|required',
            'telefono'=>'required|number',
            'email'=>'email|unique:users|required|',
            'genero'=>'required|in:masculino,femenino',
            'cumpleanios'=>'required|date',
            'password' => 'min:8|required',
            'type'=>'required|in:member,admin',
        ];
    }
}
