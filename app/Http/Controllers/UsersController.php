<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use laracast\flash\flash;
use App\Http\Requests\UserRequest;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $users = User::Search($request->nombre)->orderBy('id','ASC')->paginate(10);
        return view('users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    dd("hola desde  store");
    
    $user = new User($request->all());
    // $user->password=bcrypt($request->passworrd);
    // dd($user);
    $user->save();
    flash("Se ha registrado el usuario de forma exitosa")->success()->important();
    
    return redirect()->route('users.index');
      
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    public function show(User $user)
    {
        
        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
      
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $user=User::find($id);
       $user->fill($request->all());

       
    //    $user->foto=$request->foto;
    //    $user->documento=$request->documento;
    //    $user->nombre=$request->nombre;
    //    $user->apellido=$request->apellido;
    //    $user->telefono=$request->telefono;
    //    $user->email=$request->email;
    //    $user->genero=$request->genero;
    //    $user->cumpleanios=$request->cumpleanios;
    //    $user->interes=$request->interes;
    //    $user->type=$request->type;
       $user->save();
       flash("Se ha editado el usuario de forma exitosa")->success()->important();
       return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User:: find($id);
        $user->delete();
        flash("Se ha eliminado el usuario de forma exitosa")->error()->important();
        return redirect()->route('users.index');
    }
}
