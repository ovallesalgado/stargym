<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rutina;
class RutinasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rutinas = Rutina::orderBy('id','ASC')->paginate(10);
        return view('rutinas.index')->with('rutinas', $rutinas);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('rutinas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());


        $rutinas = new Rutina($request->all());
        // dd($rutinas);
        $rutinas->save();
       
        flash("Se ha registrado la rutina de forma exitosa")->success()->important();
        
        return redirect()->route('rutinas.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Rutina $rutina)
    {
        return view('rutinas.show')->with('rutina', $rutina);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rutinas=rutina::find($id);
        
          return view('rutinas.edit')->with('rutina', $rutinas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rutinas=Rutina::find($id);
        $rutinas->fill($request->all());
        $rutinas->save();
        flash("Se ha editado la rutina de forma exitosa")->success()->important();
        return redirect()->route('rutinas.index');
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rutinas=Rutina:: find($id);
        $rutinas->delete();
        flash("Se ha eliminado la rutina de forma exitosa")->error()->important();
        return redirect()->route('rutinas.index');
    }
}
