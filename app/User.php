<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="users";

    protected $fillable = [
        'id',
        'documento',
        'nombre',
        'apellido',
        'telefono',
        'email',
        'genero',
        'cumpleanios',
        'interes', 
        'password',
        'type',
        'remember_token',
    ];


    public function historias(){
        return $this->hasMany('App\Historia');
    }

    
    public function rutinas(){
        return $this->belongsToMany('App\Rutina');
    }

public function scopeSearch($query,$nombre){
return $query->where('nombre','LIKE',"%$nombre%");

    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}
