<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ejercicio extends Model
{
    protected $table="ejercicios";
    
    protected $fillable = [
        'id','nombre','musculo','imagen',
    ];

    public function entrenamientos(){
        return $this->hasMany('App\Entrenamientos');
    }
}

