@extends('layout.main')
@section('title','Ejercicios')
@section('content')
@include('layout.header2')
@include('flash::message')
<style>

}
</style>

<div class="container-fluid">
<div class="card ">
<div class="card-header ">
<div class="float-left"> <h4><strong>Lista de Ejercicios</strong></h4></div>


  </div>
  <div class="card-body ">

  <div class=" text-center"><a href="{{route('ejercicios.create')}}" class="btn btn-dark" alt="Registrar usuario">Registrar ejercicios</a></div>
<br>
<table class="table table-responsive-sm table-responsive-md table-responsive-lg ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
        <th scope="col">Nombre</th>
        <th scope="col">musculo</th>
        <th scope="col">Imagen</th>
        <th scope="col">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
  @foreach($ejercicios as $ejercicio)
  <tr>
      <th scope="row">{{ $ejercicio->id }}</th>
      <th>{{ $ejercicio->nombre }}</th>
      <th>{{ $ejercicio->musculo }}</th>
      <td><img src="{{ $ejercicio->imagen }}" alt="" class="img-thumbnail rounded-circle border border-secondary fotoLista" ></td>
     
    

      <th class="btn-group">
      <a href="{{route('ejercicios.show',$ejercicio->id)}}" class="btn btn-dark"><i class="fa fa-search" aria-hidden="true"></i></a>
      <a href="{{route('ejercicios.edit',$ejercicio->id)}}" class="btn btn-dark"><i class="fa fa-cogs" aria-hidden="true"></i></a>
      <a href="{{route('ejercicios.destroy',$ejercicio->id)}}" onclick="return confirm('¿Realmente desea eliminar el usuario?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </th>
    </tr>
  @endforeach   
  </tbody>
</table>
<div class="rounded float-right">
{!!$ejercicios->links()!!}
</div>
  </div>
</div>
</div>






@endsection


