@extends('layout.main')

@section('title','Registo de Rutina')
@section('content')
@include('layout.header2')

<div class="container margenB">

  


<div class=" card w-100" style="width: 20rem;">
  <div class="card-body">
    <h3 class="card-title"> <strong>Registrar Ejercicios</strong></h3>

@if(count($errors) > 0)
  <div class="alert alert-danger text-capitalize" role="alert"> 
      <ul> 
      @foreach($errors->all() as  $error)
        <li>{{$error}}</li>
      @endforeach 
      </ul>
  </div>
@endif

    {!! Form::open(['route' => 'ejercicios.store','method'=>'POST']) !!}
    
  
    
  
  
    <div class="form-group ">  
  {!! Form::label('nombre', 'Nombre'); !!}
  {!! Form::text('nombre', null, ['class'=>'form-control','required','placeholder'=>"Nombre"]); !!}
    </div>
    <div class="form-group ">  
  {!! Form::label('musculo', 'Musculo'); !!}
  {!! Form::text('musculo', null, ['class'=>'form-control','required','placeholder'=>"Nombre"]); !!}
    </div>
      
    <div class="form-group ">  
    {!! Form::label('imagen', 'Imagen'); !!}
    {!! Form::text('imagen', null, ['class'=>'form-control','placeholder'=>"http://www.mifoto.com/foto.jpg"]); !!}
      </div>
  
      <div class="form-group ">  
  {!! Form::submit('Registrar',['class'=>'btn btn-primary']);!!}
  <a class="btn btn-primary  js-scroll-trigger" href="{{route('ejercicios.index')}}">Volver</a>
  
    </div>
    
    {!! Form::close() !!}
  </div>
</div>

</div>
@endsection

<!-- 'id','foto','documento','nombre','apellido','telefono', 'email','genero','cumpleanios','interes', 'password', -->