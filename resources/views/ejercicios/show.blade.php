@extends('layout.main') @section('title','Usuarios') @section('content') @include('flash::message') @include('layout.header2')






<div class="jumbotron jumbotron-fluid margen pr-4 pl-4">
 <div class="row">
    <div class="col-md-4 text-center">
        <img src="{{ $ejercicio->imagen}}" class="img-fluid rounded-circle fotoind img-thumbnail" alt="">
        <br>
        <br>
        <a class="btn btn-primary  js-scroll-trigger  " href="{{route('ejercicios.index')}}">Volver</a>
        <br>
    </div>



    <div class="col-md-8 pt-4">
        <h3 class="text-capitalize"><strong>{{ $ejercicio->nombre }} {{ $ejercicio->apellido }}</strong>
            <br> 
            
        <hr class="hrPersonla">

        <ul class="">
            <li class="pb-2"><strong>Nombre:</strong> {{ $ejercicio->nombre }}</li>
            <li class="pb-2"><strong>Musculo:</strong> {{ $ejercicio->musculo }}</li>
           
            
        </ul>
    </div>
    </div>
</div>


@endsection
