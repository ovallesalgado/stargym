@extends('layout.main')

@section('content')
<div class="container">

<div class="card mt-5 mb-5">
  <div class="card-header">
   <strong> Registro </strong>
  </div>
  <div class="card-body">

    <div class="col-md-6 mx-auto">      
                   <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}


                       
@if(count($errors) > 0)
  <div class="alert alert-danger text-capitalize" role="alert"> 
      <ul> 
      @foreach($errors->all() as  $error)
        <li>{{$error}}</li>
      @endforeach 
      </ul>
  </div>
@endif 
                        

                        <div class="form-group{{ $errors->has('documento') ? ' has-error' : '' }}">
                            <label for="documento" class="">Documento</label>

                            <div class="">
                                <input id="documento" type="text" class="form-control" name="documento" value="{{ old('documento') }}" placeholder="1234567890" required autofocus>

                                @if ($errors->has('documento'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('documento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        


                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            <label for="nombre" class="">Nombre</label>

                            <div class="">
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" placeholder="Jhon" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                            <label for="apellido" class="">apellido</label>

                            <div class="">
                                <input id="apellido" type="text" class="form-control" name="apellido" value="{{ old('apellido') }}" placeholder="Pérez" required autofocus>

                                @if ($errors->has('apellido'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
                            <label for="telefono" class="">telefono</label>

                            <div class="">
                                <input id="telefono" type="number" class="form-control" name="telefono" value="{{ old('telefono') }}"  placeholder="1234567890" pattern="[0-9]" required autofocus>

                                @if ($errors->has('telefono'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="">E-Mail Address</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  placeholder="ejemplo@ejemplo.com" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('genero') ? ' has-error' : '' }}">
                            <label for="genero" class="">Genero</label>
                            <div class="">
                            <select class="form-control " name="genero" id="genero" required autofocus>
                                <option selected>Seleccione uno</option>
                                <option value="femenino">Femenino</option>
                                <option value="masculino">Masculino</option>
                            </select>
                              
                                @if ($errors->has('genero'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('genero') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('cumpleanios') ? ' has-error' : '' }}">
                            <label for="cumpleanios" class="">Cumpleaños</label>
                                <div class="">
                                    <input id="cumpleanios" type="date" class="form-control" name="cumpleanios" value="{{ old('cumpleanios') }}" required autofocus>
                                    @if ($errors->has('cumpleanios'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cumpleanios') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>


                        <div class="form-group{{ $errors->has('interes') ? ' has-error' : '' }}">
                            <label for="interes" class="">Interes</label>
                                <div class="">
                                    <textarea id="interes"  rows="2" class="form-control" name="interes" value="{{ old('interes') }}"  placeholder="Breve descripcion de lo que quieres lograr en STARGYM" ></textarea>
                                    @if ($errors->has('interes'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('interes') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        



                        

<br>
<hr class="hrPersonla">
<br>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="">Password</label>

                            <div class="">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="">Confirm Password</label>

                            <div class="">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                </div>
      
</div>
@endsection
