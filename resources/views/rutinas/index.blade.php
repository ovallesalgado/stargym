@extends('layout.main')
@section('title','Rutinas')
@section('content')
@include('layout.header2')
@include('flash::message')
<style>

}
</style>

<div class="container-fluid">
<div class="card ">
<div class="card-header ">
<div class="float-left"> <h4><strong>Lista de Rutinas</strong></h4></div>


  </div>
  <div class="card-body ">

  <div class=" text-center"><a href="{{route('rutinas.create')}}" class="btn btn-dark" alt="Registrar usuario">Registrar Rutinas</a></div>
<br>
<table class="table table-responsive-sm table-responsive-md table-responsive-lg ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
        <th scope="col">Nombre</th>
        <th scope="col" size=30 maxlength=30>Descripción</th>
        <th scope="col">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
  @foreach($rutinas as $rutina)
  <tr>
      <th scope="row">{{ $rutina->id }}</th>
      <th>{{ $rutina->nombre }}</th>
      <th><textarea class="textareaAncho" name="" id="" maxlength="120">{{ $rutina->descripcion }}</textarea></th>
    

      <th class="btn-group">
      <a href="{{route('rutinas.show',$rutina->id)}}" class="btn btn-dark"><i class="fa fa-search" aria-hidden="true"></i></a>
      <a href="{{route('rutinas.edit',$rutina->id)}}" class="btn btn-dark"><i class="fa fa-cogs" aria-hidden="true"></i></a>
      <a href="{{route('rutinas.destroy',$rutina->id)}}" onclick="return confirm('¿Realmente desea eliminar el usuario?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </th>
    </tr>
  @endforeach   
  </tbody>
</table>
<div class="rounded float-right">
{!!$rutinas->links()!!}
</div>
  </div>
</div>
</div>






@endsection


