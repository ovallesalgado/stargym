@extends('layout.main') 
@section('title','Rutina')
 @section('content') 
 @include('flash::message') 
 @include('layout.header2')






<div class="jumbotron jumbotron-fluid margen pr-4 pl-4">
 <div class="row">
    <div class="col-md-4 text-center">
    <h3 class="text-capitalize"><strong>{{ $rutina->nombre }} {{ $rutina->apellido }}</strong>
        <br>
        <br>
        <a class="btn btn-primary  js-scroll-trigger  " href="{{route('rutinas.index')}}">Volver</a>
        <br>
        <br>  
    </div>

    <div class="col-md-8 pt-4">

        <p ><textarea name="" id="" class="text-justify textareaAncho"  rows="4">{{ $rutina->descripcion }}</textarea></p>
        <ul class="">
          
        </ul>
    </div>
    </div>
</div>


@endsection
