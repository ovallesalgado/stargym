@extends('layout.main') @section('title','Usuarios') @section('content') @include('flash::message') @include('layout.header2')






<div class="jumbotron jumbotron-fluid margen pr-4 pl-4">
 <div class="row">
    <div class="col-md-4 text-center">
    <h3 class="text-capitalize"><strong>{{ $user->nombre }} {{ $user->apellido }}</strong>
        <br>
        (<small>{{ $user->cumpleanios}}</small>)</h3>
        <br>
        <a class="btn btn-primary  js-scroll-trigger  " href="{{route('users.index')}}">Volver</a>
        <br>
    </div>



    <div class="col-md-8 pt-2">
               
        <hr class="hrPersonla">
        <p ><textarea name="" id="" class="text-justify textareaAncho"  rows="3">{{ $user->interes }}</textarea></p>
        <ul class="">
            <li class="pb-1"><strong>Documento:</strong> {{ $user->documento }}</li>
            <li class="pb-1"><strong>Telefono:</strong> {{ $user->telefono }}</li>
            <li class="pb-1"><strong>Email:</strong> {{ $user->email }}</li>
            <li class="pb-1"><strong>Genero:</strong> {{ $user->genero }}</li>
            <li class="pb-1"><strong>Cumpleaños:</strong> {{ $user->cumpleanios }}</li>
        </ul>
    </div>
    </div>
</div>


@endsection
