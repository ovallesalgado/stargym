@extends('layout.main')

@section('title','Editar Usuario')
@section('content')
@include('layout.header2')

<div class="container margenB">

  


<div class="animated zoomIn card w-100" style="width: 20rem;">
  <div class="card-body">
    <h3 class="card-title"> <strong>Editar usuario {{$user->nombre}}</strong></h3>

    {!! Form::open(['route' => ['users.update', $user],'method'=>'PUT']) !!}



    <div class="form-group ">  
  {!! Form::label('documento', 'Documento'); !!}
  {!! Form::text('documento', $user->documento, ['class'=>'form-control','required','placeholder'=>"Documento"]); !!}
    </div>
  
  
    
  
  
    <div class="form-group ">  
  {!! Form::label('nombre', 'Nombre'); !!}
  {!! Form::text('nombre', $user->nombre, ['class'=>'form-control','required','placeholder'=>"Nombre"]); !!}
    </div>
    <div class="form-group ">  
  {!! Form::label('apellido', 'Apellido'); !!}
  {!! Form::text('apellido', $user->apellido, ['class'=>'form-control','required','placeholder'=>"Apellido"]); !!}
    </div>
    <div class="form-group ">  
  {!! Form::label('telefono', 'Telefono'); !!}
  {!! Form::tel('telefono', $user->telefono, ['class'=>'form-control','required','placeholder'=>"Telefono"]); !!}
    </div>
    <div class="form-group ">  
    {!! Form::label('email', 'Correo Electronico'); !!}
  {!! Form::email('email', $user->email, ['class'=>'form-control','required','placeholder'=>"correo@correo.com"]); !!}
    </div>
    
    <div class="form-group ">  
        {!! Form::label('genero', 'Genero'); !!}
        {!! Form::select('genero', ['' => 'Selecione Uno','femenino' => 'Femenino', 'masculino' => 'Masculino'],$user->genero,['class'=>'form-control','required',]) !!}
      </div>
      
      <div class="form-group ">  
          {!! Form::label('cumpleanios', 'Cumpleaños'); !!}
          {!! Form::date('cumpleanios', $user->cumpleanios, ['class'=>'form-control','required','placeholder'=>"correo@correo.com"]); !!}
      </div>
      
      <div class="form-group ">  
      {!! Form::label('interes', 'Interes'); !!}
      {!! Form::textarea('interes',$user->interes, ['class'=>'form-control','size' => '30x4','required','placeholder'=>"Interes"]); !!}
      </div>
  
    
   
  
    <div class="form-group ">  
  {!! Form::label('type', 'Tipo'); !!}
  {!! Form::select('type', ['' => 'Selecione Uno','member' => 'Usuario','admin' => 'Administrador'],$user->type,['class'=>'form-control','required', ]) !!}
    </div>
  
    <div class="form-group ">  
  {!! Form::submit('Editar',['class'=>'btn btn-primary']);!!}
  <a class="btn btn-primary  js-scroll-trigger" href="{{route('users.index')}}">Volver</a>
  
    </div>
    
    {!! Form::close() !!}
  </div>
</div>

</div>
@endsection