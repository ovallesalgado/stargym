@extends('layout.main')
@section('title','Usuarios')
@section('content')
@include('layout.header2')
@include('flash::message')
<style>

}
</style>

<div class="container-fluid">
<div class="card ">
<div class="card-header ">
<div class="float-left"> <h4><strong>Lista de Usuarios</strong></h4></div>


  </div>
  <div class="card-body ">
<!-- buscador -->

    
{!! Form::open(['route' =>['users.index'],'method'=>'GET','class'=>'navbar-form pull-right'])!!}
<div class="input-group">
{!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Buscar...','aria-describedby="search"']) !!}
<span class="input-group-addon" id="search"><i class="fa fa-search" aria-hidden="true"></i></span>
</div>

{!! Form::close() !!}
<br>
<!-- fin buscador -->
  <!-- <div class=" text-center"><a href="{{ route('register') }}" class="btn btn-dark" alt="Registrar usuario">Registrar usuario</a></div> -->
<br>
<div class="rounded float-right">
{!!$users->links()!!}
</div>
<table class="table table-responsive-sm table-responsive-md table-responsive-lg ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
    
      <th scope="col">Documento</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellido</th>
      <th scope="col">Telefono</th>
      <th scope="col">Email</th>
      <th scope="col">Tipo</th>
      <th scope="col">Acciones</th>
      
    </tr>
  </thead>
  <tbody>
  @foreach($users as $user)
  <tr>
      <th scope="row">{{ $user->id }}</th>
    
      <th >{{ $user->documento }}</th>
      <th>{{ $user->nombre }}</th>
      <th>{{ $user->apellido }}</th>
      <th>{{ $user->telefono }}</th>
      <th>{{ $user->email }}</th>
      <th>
      @if($user->type=="admin")
      <span class="badge badge-primary">{{ $user->type}}</span>
      @else
      <span class="badge badge-info">{{ $user->type}}</span>
      @endif
      </th>
      <th class="btn-group">
      <a href="{{route('users.show',$user->id)}}" class="btn btn-dark"><i class="fa fa-search" aria-hidden="true"></i></a>
      <a href="{{route('users.edit',$user->id)}}" class="btn btn-dark"><i class="fa fa-cogs" aria-hidden="true"></i></a>
      <a href="{{route('users.destroy',$user->id)}}" onclick="return confirm('¿Realmente desea eliminar el usuario?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
      </th>
    </tr>
  @endforeach   
  </tbody>
</table>
<div class="rounded float-right">
{!!$users->links()!!}
</div>
  </div>
</div>
</div>






@endsection


